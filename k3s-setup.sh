#!/usr/bin/env bash
set -euo pipefail
# set -euox pipefail
# Install K3s and helm

DOTFILES=$(dirname -- "$(readlink -f "$0")")"/"

install_k3s() {
  pushd $(mktemp -d)
	INSTALLER_PATH="./install_k3s.sh"
	curl \
		--silent \
		--show-error \
		--fail \
		--location \
		--output "$INSTALLER_PATH" \
		https://get.k3s.io
	chmod 700 "$INSTALLER_PATH"
	"$INSTALLER_PATH" server \
		--cluster-init
  popd
}

copy_k3s_config() {
	mkdir -p "${HOME}/.kube" &&
		sudo cp /etc/rancher/k3s/k3s.yaml "${HOME}/.kube/config" &&
		sudo chown --recursive "$(id -u)":"$(id -g)" "${HOME}/.kube"

	grep -q 'KUBECONFIG' "${HOME}/.zprofile" ||
		cat <<-'EOF' >> "${HOME}/.zprofile"
			            
    # Set KUBECONFIG to the local file
    export KUBECONFIG=~/.kube/config
    alias kubectl="sudo -E /usr/local/bin/kubectl"
    alias helm="sudo -E /usr/local/bin/helm"
		EOF
	#   sudo tee /etc/sudoers.d/k3s <<- EOF
	#   Defaults env_keep += "http_proxy https_proxy no_proxy"
	#   Defaults env_keep += "HTTP_PROXY HTTPS_PROXY NO_PROXY"
	#   Defaults env_keep += "KUBECONFIG"
	# EOF
}

install_helm() {
  pushd $(mktemp -d)
	INSTALLER_PATH="./install_helm.sh"
	curl --silent \
		--show-error \
		--fail \
		--location \
		--output "$INSTALLER_PATH" \
		https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
	chmod 700 "$INSTALLER_PATH"
	"$INSTALLER_PATH"
  popd
}

root_local_bin() {
	sudo mkdir -p /root/.local/bin
	sudo ln -sfv /usr/local/bin/* /root/.local/bin/
}

install_k3s &&
	copy_k3s_config &&
	install_helm # &&
# root_local_bin
