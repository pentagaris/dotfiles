# Path Stuff
# export PATH="/snap/bin:$PATH"
grep -q '/usr/local/bin' <<<"$PATH" || export PATH="$PATH:/usr/local/bin"
grep -q "$HOME/.local/bin" <<<"$PATH" || export PATH="$HOME/.local/bin:$PATH"
grep -q "$HOME/.cargo/bin" <<<"$PATH" || export PATH="$HOME/.cargo/bin:$PATH"
grep -q "$HOME/.npm-packages/bin" <<<"$PATH" || export PATH="$HOME/.npm-packages/bin:$PATH"
