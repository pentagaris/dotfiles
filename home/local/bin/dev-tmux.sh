#!/bin/sh
tmux new-session -s "DevTmux" \; \
	send-keys 'nvim ~/workspace' C-m \; \
	split-window -v -p  10 \; \
	select-pane -t 1 \;
