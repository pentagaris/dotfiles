#!/usr/bin/env python3
import sys


def spongebob_words(words):
    count = 0
    if len(words) == 1:
        words = words[0].split()
    for word in words:
        for letter in word:
            if count % 2 != 0:
                print(f"{letter.upper()}", end="")
            else:
                print(f"{letter.lower()}", end="")
            count += 1

        print(" ", end="")


if __name__ == "__main__":
    spongebob_words(sys.argv[1:])
