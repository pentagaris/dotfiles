#! /usr/bin/bash
echo "###### Z F S ######"
echo "Identify the disks id pattern, i.e. 'usb-JMicron_Generic_DISK0*'"
echo ""
read -rp "Press any key to list disks by id..."
ls /dev/disk/by-id/
read -rp "pattern=" pattern
echo "Counting number of drives found with $pattern"
count="$(find /dev/disk/by-id/ -regextype sed -regex ".*/${pattern}.*" -not -regex ".*-part[0-9]*" | wc -l)"
echo "$pattern returned $count drives, is this correct?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) echo "Generating autommount script...";break;;
        No ) echo "Exiting, start this script again";exit;;
    esac
done

cat << EOF > /usr/local/bin/waitZFSMount.sh
#! /usr/bin/bash
while [ ! -d /sys/module/zfs ];
do
  sleep 1;
done
echo zfs module is loaded
while [ ! "$(find /dev/disk/by-id/ -regextype sed -regex ".*/${pattern}.*" -not -regex ".*-part[0-9]*" | wc -l)" = "$count" ];
do
  sleep 1
done
echo USB disks are ready
EOF

cat << EOF > /etc/systemd/system/zfs-custom-mount.service
[Unit]
Description=Mount ZFS filesystems (custom edition)
Documentation=man:zfs(8)
DefaultDependencies=no
After=systemd-udev-settle.service

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStartPre=/usr/local/bin/waitZFSMount.sh
ExecStart=/usr/bin/zpool import -a

[Install]
WantedBy=multi-user.target
EOF


systemctl daemon-reload
systemctl enable --now /etc/systemd/system/zfs-custom-mount.service
systemctl start zfs-custom-mount
systemctl status zfs-custom-mount
