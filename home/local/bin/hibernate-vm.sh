#!/bin/bash

#
# Usage: hibernate-vm NAME
#
# Hibernates the VM specified in NAME and waits for it to finish shutting down
#

if virsh save "$1" disk; then
	echo "Waiting for domain to finish shutting down.." >&2
	while ! [ "$(virsh domstate "$1")" == 'shut off' ]; do
		sleep 1
	done
	echo "Domain finished shutting down" >&2
fi
