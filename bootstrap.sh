#!/usr/bin/env bash
set -euo pipefail
# set -euox pipefail

# DOTFILES=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd)
DOTFILES=$(dirname -- "$(readlink -f "$0")")"/"

echo "${DOTFILES}" && cd "${DOTFILES}"
# This shouldn't happen anymore, but keeping it here in case I change things later
if [[ ! -f "${DOTFILES}"/.isdotfiles ]]; then
    echo "Not in the dotfiles directory!"
    exit
fi

source "./bsfunctions.sh"

DESKTOP='false'
print_usage() {
    printf "Usage: ..."
}

while getopts 'd' flag; do
    case "${flag}" in
    d) DESKTOP='true' ;;
    *)
        print_usage
        exit 1
        ;;
    esac
done

COMMON_BASE_PACKAGES=(
    cifs-utils
    cmake
    crun
    curl
    gcc
    gettext
    jq
    make
    man-db
    podman
    powerline
    rsync
    shellcheck
    tmux
    unzip
    usbutils
    vim
    wget
    zsh
)

ARCH_BASE_PACKAGES=(
    avahi
    base-devel
    ninja-build
    nss-mdns
    python-pipx
    xz
)

DEBIAN_BASE_PACKAGES=(
    avahi-daemon
    build-essential
    libnss-mdns
    ninja-build
    pipx
    xz-utils
)

RHEL_BASE_PACKAGES=(
    avahi
    avahi-tools
    ninja-build
    nss-mdns
    pipx
    samba-client
    xz
)

COMMON_DESKTOP_PACKAGES=(
    firefox
    flatpak
    fprintd
    # fuse
    rxvt-unicode
)

YAY_PACKAGES=(
    1password
    lunatask
    pcloud-drive
)

#########################################################
# Main
# Check if the system is a MacOs or Linux
# If Linux, check if it's Ubuntu, RedHat, or Arch
#########################################################

# If MacBook, install brew and neovim
if uname -a | grep -qi darwin; then
    echo "Found MacOs"
    # install Brew
    if ! command -v brew &>/dev/null; then
        (
            cd "$(mktemp -d) || exit"
            wget https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh -O ./brew_install.sh
            chmod +x ./brew_install.sh
            ./brew_install.sh
        )
    fi

    brew install \
        cmake \
        curl \
        gettext \
        jq \
        ninja

    install_asdf
    get_nerd_font
    set_npm_global_as_home
    install_neolunarvim macos
    install_shellcheck darwin
    install_omz
    create_symlinks
    get_authorized_keys
    change_dotfiles_origin
    change_to_zsh

else
    # Welcome to the Debian Cult
    if command -v apt &>/dev/null; then
        echo "Found a Debian-based System"

        sudo apt-get update -y
        sudo NEEDRESTART_MODE="a" apt-get upgrade -y
        sudo apt-get install -y "${COMMON_BASE_PACKAGES[@]}" "${DEBIAN_BASE_PACKAGES[@]}"
        if [[ "${DESKTOP}" == "true" ]]; then
            sudo apt-get install -y "${COMMON_DESKTOP_PACKAGES[@]}"
            pushd "$(mktemp -d)"
            wget -c https://downloads.1password.com/linux/debian/amd64/stable/1password-latest.deb
            sudo apt-get install -y ./1password-latest.deb
            sign_with_1p
            popd
        fi

    # M'lady
    elif command -v dnf &>/dev/null; then
        echo "Found a RedHat based system"

        sudo dnf config-manager --set-enabled crb
        sudo dnf install epel-release -y
        sudo dnf update -y
        sudo dnf group install "Development Tools" \
            "Server" \
            "System Tools" -y
        sudo dnf install -y "${COMMON_BASE_PACKAGES[@]}" "${RHEL_BASE_PACKAGES[@]}"
        if [[ "${DESKTOP}" == "true" ]]; then
            sudo dnf install -y "${COMMON_DESKTOP_PACKAGES[@]}"
            pushd "$(mktemp -d)"
            wget -c https://downloads.1password.com/linux/rpm/stable/x86_64/1password-latest.rpm
            sudo dnf install -y ./1password-latest.rpm
            sign_with_1p
        fi

    # I use Arch, btw
    elif command -v pacman &>/dev/null; then
        echo "Found Arch"

        sudo pacman --sync \
            --noconfirm \
            --refresh \
            --sysupgrade

        sudo pacman --sync \
            --noconfirm \
            --needed \
            "${COMMON_BASE_PACKAGES[@]}" "${ARCH_BASE_PACKAGES[@]}"

        if [[ "${DESKTOP}" == "true" ]]; then
            echo "Adding 1Password GPG Key"
            curl -sS https://downloads.1password.com/linux/keys/1password.asc | gpg --import
            install_yay
            yay --sync \
                --noconfirm \
                --needed \
                "${COMMON_DESKTOP_PACKAGES[@]}" "${YAY_PACKAGES[@]}"
            sign_with_1p
        fi

    else
        echo "No supported platforms found!" && exit 1
    fi
    echo "Installing the rest of our stuff..."
    linux_install_base
    [[ "${DESKTOP}" == "true" ]] &&
        linux_install_desktop
    echo "Installation completed. Make sure to install Lunatask and pCloud manually!"
fi
echo "Done!"
