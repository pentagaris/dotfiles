#!/usr/bin/env bash
set -euo pipefail
# set -euox pipefail

put_gitconfig() {
    cat <<EOF >"${HOME}"/.gitconfig
[user]
        email = justin@mygoodsir.wtf
        name =  Justin C
        signingkey = ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJaL657IO1hSefgBDqBiJBj9kMcgIXbbJ9q3EDQYWCiy
[gpg]
        format = ssh
[gpg "ssh"]
        program = "ssh-agent"
[commit]
        gpgsign = true

[color]
        ui = auto
[core]
        pager = less -FMRiX
        autocrlf = input
        editor = vim
        sshCommand = ssh
[push]
        default = simple
[rerere]
        enabled = true
[pull]
        rebase = true
[init]
        defaultBranch = main
[alias]
        dag = log --graph --format='format:%C(yellow)%h%C(reset) %C(blue)\"%an\" <%ae>%C(reset) %C(magenta)%cr%C(reset)%C(auto)%d%C(reset)%n%s' --date-order
        yeet = push
        yoink = pull
        yolo = push --force
        rekt = reset --hard HEAD
EOF

    command -v "ssh.exe" && git config --global core.sshCommand "ssh.exe"
    command -v "vim" && git config --global core.editor "vim"
    command -v "nvim" && git config --global core.editor "nvim"
}

get_authorized_keys() {
    curl --silent \
        --show-error \
        --fail \
        --output "${HOME}/.ssh/authorized_keys" \
        --location \
        "https://github.com/pentagaris.keys"
}

change_dotfiles_origin() {
    (
        cd "${DOTFILES}"
        git remote -v | grep -q https &&
            git remote set-url origin git@gitlab.com:pentagaris/dotfiles.git &&
            git branch --set-upstream-to=origin/main main
    )
    echo "Dotfiles remote is now: $(git remote -v)"
}

install_rust() {
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --no-modify-path
}

get_flatpaks() {
    command -v flatpak &&
        flatpak remote-delete flathub &&
        flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo &&
        echo "Installing Flatpaks" &&
        flatpak install \
            ch.protonmail.protonmail-bridge \
            com.protonvpn.www \
            org.gimp.GIMP \
            org.mozilla.Thunderbird \
            com.slack.Slack \
            org.signal.Signal \
            com.discordapp.Discord \
            --noninteractive || echo "Can't Install Flatpaks"
}

get_nerd_font() {
    (
        pushd "$(mktemp -d)"
        git clone --filter=blob:none --sparse https://github.com/ryanoasis/nerd-fonts.git
        cd nerd-fonts
        git sparse-checkout add patched-fonts/FantasqueSansMono
        ./install.sh -s FantasqueSansMono
        popd
    )
}

set_npm_global_to_home() {
    NPM=$(command -v npm)
    NPM_HOME="$HOME/.npm-packages"
    mkdir -p "$NPM_HOME"
    "$NPM" config set prefix --location user "$NPM_HOME"
}

install_kitty() {
    cd "${DOTFILES}"
    touch "${DOTFILESLOCAL}/bin/kitty_install.sh"
    wget --verbose https://sw.kovidgoyal.net/kitty/installer.sh -O "${DOTFILESLOCAL}/bin/kitty_install.sh"
    file "${DOTFILESLOCAL}/bin/kitty_install.sh"
    git diff "${DOTFILESLOCAL}/bin/kitty_install.sh" &&
        chmod +x "${DOTFILESLOCAL}/bin/kitty_install.sh" &&
        "${DOTFILESLOCAL}/bin/kitty_install.sh"
    touch "${DOTFILES}/local/bin/kitty_install.sh"
    wget --verbose https://sw.kovidgoyal.net/kitty/installer.sh -O "${DOTFILES}/local/bin/kitty_install.sh"
    file "${DOTFILES}/local/bin/kitty_install.sh"
    git diff "${DOTFILES}/local/bin/kitty_install.sh" &&
        chmod +x "${DOTFILES}/local/bin/kitty_install.sh" &&
        "${DOTFILES}/local/bin/kitty_install.sh"

    # Create symbolic links to add kitty and kitten to PATH (assuming ~/.local/bin is in your system-wide PATH)
    ln -sf ~/.local/kitty.app/bin/kitty ~/.local/kitty.app/bin/kitten ~/.local/bin/
    # Place the kitty.desktop file somewhere it can be found by the OS
    mkdir -p ~/.local/share/applications
    cp ~/.local/kitty.app/share/applications/kitty.desktop ~/.local/share/applications/
    # If you want to open text files and images in kitty via your file manager also add the kitty-open.desktop file
    cp ~/.local/kitty.app/share/applications/kitty-open.desktop ~/.local/share/applications/
    # Update the paths to the kitty and its icon in the kitty desktop file(s)
    sed -i "s|Icon=kitty|Icon=$(readlink -f ~)/.local/kitty.app/share/icons/hicolor/256x256/apps/kitty.png|g" ~/.local/share/applications/kitty*.desktop
    sed -i "s|Exec=kitty|Exec=$(readlink -f ~)/.local/kitty.app/bin/kitty|g" ~/.local/share/applications/kitty*.desktop
    # Make xdg-terminal-exec (and hence desktop environments that support it use kitty)
    echo 'kitty.desktop' >~/.config/xdg-terminals.list
}

install_neovim() {
    pushd "$(mktemp -d)"
    git clone https://github.com/neovim/neovim
    cd neovim
    git checkout stable
    rm -rf build/ # clear the CMake cache
    make CMAKE_EXTRA_FLAGS="-DCMAKE_INSTALL_PREFIX=/opt/neovim" CMAKE_BUILD_TYPE="Release"
    sudo make install
    sudo ln -sfv "/opt/neovim/bin/nvim" "/usr/local/bin/"
    sudo ln -sfv "/opt/neovim/bin/nvim" "$HOME/.local/bin/"
    popd
}

install_lunarvim() {
    pushd "$(mktemp -d)"
    curl --silent \
        --show-error \
        --fail \
        --location \
        https://raw.githubusercontent.com/lunarvim/lunarvim/master/utils/installer/install.sh -o "./lvim_install.sh" &&
        chmod +x "./lvim_install.sh" &&
        "./lvim_install.sh" --overwrite --yes
    popd
}

install_awscli() {
    cd "$(mktemp -d)"
    curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "./awscliv2.zip"
    unzip "./awscliv2.zip"
    ./aws/install -i "${HOME}/.local/aws-cli" --update -b "${HOME}/.local/bin"
}

# install asdf & install relevant stuff
install_asdf() {

    ASDF_PATH="${HOME}/.asdf"

    GOLANG_LATEST=$(curl -sL 'https://go.dev/VERSION?m=text' | head -n1 | sed 's/^go//')
    NODEJS_LATEST=$(curl -sL 'https://nodejs.org/download/release/index.json' |
        jq -r '[.[] | select(.lts!=false)][2] | .version' |
        sed 's/^v//')
    PATH="${ASDF_PATH}/bin:${PATH}"
    if [[ ! -d "${ASDF_PATH}" ]]; then
        echo -n "Installing asdf"
        ASDF_LATEST_TAG=$(curl -sqL https://api.github.com/repos/asdf-vm/asdf/releases/latest |
            jq -r .tag_name)
        git clone 'https://github.com/asdf-vm/asdf.git' "$ASDF_PATH" --branch "$ASDF_LATEST_TAG"
    fi

    source "${ASDF_PATH}/asdf.sh"
    # python
    asdf plugin add python
    asdf global python system
    # node
    asdf plugin add nodejs
    asdf install nodejs "${NODEJS_LATEST}"
    asdf global nodejs "${NODEJS_LATEST}"
    # rust
    asdf plugin add rust
    asdf install rust stable
    asdf global rust stable
    # golang
    asdf plugin add golang
    asdf install golang "${GOLANG_LATEST}"
    asdf global golang "${GOLANG_LATEST}"

    # Install wheel, pipenv, and mkdir on npm prefix
    NPM_PREFIX=$("${ASDF_PATH}/shims/npm" config get prefix)
    mkdir -pv "${NPM_PREFIX}"
}

install_rust_utils() {
    for util in stylua; do
        command -v "$util" || cargo install "$util"
    done
}

install_node_utils() {
    for util in prettier eslint; do
        command -v "$util" || npm install --global "$util"
    done
}

install_python_utils() {
    pipx ensurepath
    for util in poetry podman-compose; do
        # for util in flake8 black isort codespell poetry podman-compose; do
        command -v "$util" || pipx install "$util"
    done
}

install_go_utils() {
    # shfmt
    command -v shfmt ||
        (
            GOPATH="${HOME}/.local/shfmt" go install "mvdan.cc/sh/v3/cmd/shfmt@latest"
            ln -sfv "${HOME}/.local/shfmt/bin/shfmt" "${HOME}/.local/bin/"
        )

    # lazygit
    command -v lazygit ||
        (
            pushd "$(mktemp -d)"
            git clone https://github.com/jesseduffield/lazygit.git
            cd lazygit
            GOPATH="${HOME}/.local/lazygit" go install
            ln -sfv "${HOME}/.local/lazygit/bin/lazygit" "${HOME}/.local/bin/"
            popd
        )
}

install_yay() {
    sudo pacman -Sy --needed --noconfirm git base-devel
    pushd "$(mktemp -d)"
    git clone https://aur.archlinux.org/yay.git
    cd yay
    makepkg --syncdeps \
        --install \
        --noconfirm
    popd
}

#Install oh-my-zsh
install_omz() {
    echo -n "Linking ~/.oh-my-zsh: "
    if [[ ! -d "${HOME}"/.oh-my-zsh ]]; then
        curl -Lo /tmp/zsh_install.sh \
            https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh
        chmod +x /tmp/zsh_install.sh && /tmp/zsh_install.sh --unattended
    else
        echo "${HOME}/.oh-my-zsh is already installed!"
    fi
}
create_symlinks() {
    echo "Creating directories in ${HOME}: "

    # Move to the home subdir
    pushd "${DOTFILES}/home"
    find -- * -type d \
        -print0 | xargs -0 -I dir mkdir -pv "${HOME}/.dir"
    echo "Creating symlinks new directories: "
    find -- * -type f \
        -print0 | xargs -0 -I filepath ln -sfv "${PWD}/filepath" "${HOME}/.filepath"
    popd

    # links tmux to the root for old versions
    ln -sfv "${HOME}/.config/tmux/tmux.conf" "${HOME}/.tmux.conf"
}

change_to_zsh() {
    export tempuser &&
        tempuser="$(whoami)" &&
        sudo chsh --shell "$(command -v zsh)" "$tempuser"
}

sign_with_1p() {
    if uname -a | grep -iq "microsoft"; then
        git config --global gpg.ssh.program "/mnt/c/Users/justinmc/AppData/Local/1Password/app/8/op-ssh-sign-wsl"
    elif uname -a | grep -iq "darwin"; then
        git config --global gpg.ssh.program "/Applications/1Password.app/Contents/MacOS/op-ssh-sign"
    else
        git config --global gpg.ssh.program "/opt/1Password/op-ssh-sign"
    fi
}

linux_install_base() {
    install_asdf
    set_npm_global_to_home
    install_python_utils
    # install_node_utils
    # install_rust_utils
    # install_go_utils
    command -v nvim || install_neovim
    put_gitconfig
    # install_lunarvim
    # install_awscli
    install_omz
    create_symlinks
    get_authorized_keys
    change_dotfiles_origin
    change_to_zsh
}

linux_install_desktop() {
    get_nerd_font
    # install_kitty
}

linux_install_interactive() {
    get_nerd_font
    # install_kitty
    get_flatpaks
}
